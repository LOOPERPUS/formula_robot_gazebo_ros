#include "formula_robot_gazebo_ros/formula_drive.h"

FormulaDrive::FormulaDrive()
  : nh_priv_("~")
{
  ROS_ASSERT(init());
}

FormulaDrive::~FormulaDrive()
{
  updatecommandVelocity(0.0, 0.0);
  ros::shutdown();
}

bool FormulaDrive::init()
{
  std::string cmd_vel_topic_name(nh_.param<std::string>("cmd_vel_topic_name", ""));

  escape_range_       = 30.0 * DEG2RAD;
  check_forward_dist_ = 0.7;
  check_side_dist_    = 0.6;

  formula_pose_ = 0.0;
  prev_formula_pose_ = 0.0;

  cmd_vel_pub_ = nh_.advertise<geometry_msgs::Twist>(cmd_vel_topic_name, 10);

  laser_scan_sub_  = nh_.subscribe("scan", 10, &FormulaDrive::laserScanMsgCallBack, this);
  odom_sub_ = nh_.subscribe("odom", 10, &FormulaDrive::odomMsgCallBack, this);

  return true;
}

void FormulaDrive::odomMsgCallBack(const nav_msgs::Odometry::ConstPtr &msg)
{
  double siny(2.0 * (msg->pose.pose.orientation.w * msg->pose.pose.orientation.z + msg->pose.pose.orientation.x * msg->pose.pose.orientation.y));
	double cosy(1.0 - 2.0 * (msg->pose.pose.orientation.y * msg->pose.pose.orientation.y + msg->pose.pose.orientation.z * msg->pose.pose.orientation.z));  

	formula_pose_ = atan2(siny, cosy);
}

void FormulaDrive::laserScanMsgCallBack(const sensor_msgs::LaserScan::ConstPtr &msg)
{
  uint16_t scan_angle[3]({0, 30, 330});

  for (size_t num (0); num < 3; num+=1)
  {
    if (std::isinf(msg->ranges.at(scan_angle[num])))
    {
      scan_data_[num] = msg->range_max;
    }
    else
    {
      scan_data_[num] = msg->ranges.at(scan_angle[num]);
    }
  }
}

void FormulaDrive::updatecommandVelocity(double linear, double angular)
{
  geometry_msgs::Twist cmd_vel;

  cmd_vel.linear.x  = linear;
  cmd_vel.angular.z = angular;

  cmd_vel_pub_.publish(cmd_vel);
}

bool FormulaDrive::controlLoop()
{
  static uint8_t formula_state_num(0);

  switch(formula_state_num)
  {
    case GET_FORMULA_DIRECTION:
      if (scan_data_[CENTER] > check_forward_dist_)
      {
        if (scan_data_[LEFT] < check_side_dist_)
        {
          prev_formula_pose_ = formula_pose_;
          formula_state_num = FORMULA_RIGHT_TURN;
        }
        else if (scan_data_[RIGHT] < check_side_dist_)
        {
          prev_formula_pose_ = formula_pose_;
          formula_state_num = FORMULA_LEFT_TURN;
        }
        else
        {
          formula_state_num = FORMULA_DRIVE_FORWARD;
        }
      }

      if (scan_data_[CENTER] < check_forward_dist_)
      {
        prev_formula_pose_ = formula_pose_;
        formula_state_num = FORMULA_RIGHT_TURN;
      }
      break;

    case FORMULA_DRIVE_FORWARD:
      updatecommandVelocity(LINEAR_VELOCITY, 0.0);
      formula_state_num = GET_FORMULA_DIRECTION;
      break;

    case FORMULA_RIGHT_TURN:
      if (fabs(prev_formula_pose_ - formula_pose_) >= escape_range_)
        formula_state_num = GET_FORMULA_DIRECTION;
      else
        updatecommandVelocity(0.0, -1 * ANGULAR_VELOCITY);
      break;

    case FORMULA_LEFT_TURN:
      if (fabs(prev_formula_pose_ - formula_pose_) >= escape_range_)
        formula_state_num = GET_FORMULA_DIRECTION;
      else
        updatecommandVelocity(0.0, ANGULAR_VELOCITY);
      break;

    default:
      formula_state_num = GET_FORMULA_DIRECTION;
      break;
  }

  return true;
}

int main(int argc, char* argv[])
{
  ros::init(argc, argv, "formula_drive");
  FormulaDrive formula_drive;

  ros::Rate loop_rate(125);

  while (ros::ok())
  {
    formula_drive.controlLoop();
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
