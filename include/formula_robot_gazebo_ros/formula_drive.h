#ifndef FORMULA_DRIVE_H_
#define FORMULA_DRIVE_H_

#include <ros/ros.h>

#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>

#define DEG2RAD (M_PI / 180.0)
#define RAD2DEG (180.0 / M_PI)

#define CENTER 0
#define LEFT   1
#define RIGHT  2

#define LINEAR_VELOCITY  0.3
#define ANGULAR_VELOCITY 1.5

#define GET_FORMULA_DIRECTION 0
#define FORMULA_DRIVE_FORWARD 1
#define FORMULA_RIGHT_TURN    2
#define FORMULA_LEFT_TURN     3

class FormulaDrive
{
 public:
  FormulaDrive();
  ~FormulaDrive();

  bool init();
  bool controlLoop();
 private:
  ros::NodeHandle nh_;
  ros::NodeHandle nh_priv_;

  ros::Publisher cmd_vel_pub_;

  ros::Subscriber laser_scan_sub_;
  ros::Subscriber odom_sub_;

  double escape_range_;
  double check_forward_dist_;
  double check_side_dist_;

  double scan_data_[3] = {0.0, 0.0, 0.0};

  double formula_pose_;
  double prev_formula_pose_;

  void updatecommandVelocity(double linear, double angular);
  void laserScanMsgCallBack(const sensor_msgs::LaserScan::ConstPtr &msg);
  void odomMsgCallBack(const nav_msgs::Odometry::ConstPtr &msg);
};
#endif // FORMULA_DRIVE_H_
